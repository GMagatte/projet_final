DROP DATABASE Projet_Final;
CREATE DATABASE Projet_Final;

USE Projet_Final;

DROP TABLE IF EXISTS user_table;
CREATE TABLE user_table (
    user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    userName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL,
    password VARCHAR(255) NOT NULL,
    createdAt DATE
);

DROP TABLE IF EXISTS anime_table;

CREATE TABLE anime_table (
    anime_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    animeName VARCHAR(255) NOT NULL,
    episodeNumber INTEGER NOT NULL,
    seasonNumber INTEGER NOT NULL,
    description TEXT NOT NULL,
    createdAt DATE
);

DROP TABLE IF EXISTS list_table;

CREATE TABLE list_table (
    list_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    listName VARCHAR(255) NOT NULL,
    createdAt DATE
) ;

-- DROP TABLE IF EXISTS comment_table;

CREATE TABLE comment_table (
    comment_id INTEGER AUTO_INCREMENT,
    comment_text TEXT NOT NULL,
    user_id INTEGER NOT NULL,
    anime_id INTEGER NOT NULL,
    PRIMARY KEY(comment_id,user_id,anime_id),
    FOREIGN KEY (user_id) REFERENCES user_table(user_id),
    FOREIGN KEY (anime_id) REFERENCES anime_table(anime_id)
);

DROP TABLE IF EXISTS user_list;

-- CREATE TABLE IF NOT EXISTS user_list(
--     user_id INTEGER NOT NULL,
--     list_id INTEGER NOT NULL,
--     PRIMARY KEY(user_id,list_id),
--     FOREIGN KEY (user_id) REFERENCES user_table(user_id),
--     FOREIGN KEY (list_id) REFERENCES list_table(list_id)
-- );

DROP TABLE IF EXISTS user_comment;

CREATE TABLE IF NOT EXISTS user_comment(
    user_id INTEGER NOT NULL,
    comment_id INTEGER NOT NULL,
    PRIMARY KEY(user_id,comment_id),
    FOREIGN KEY (user_id) REFERENCES user_table(user_id),
    FOREIGN KEY (comment_id) REFERENCES comment_table(comment_id)
);

DROP TABLE IF EXISTS anime_list;

-- CREATE TABLE IF NOT EXISTS anime_list(
--     anime_id INTEGER NOT NULL,
--     list_id INTEGER NOT NULL,
--     PRIMARY KEY(anime_id,list_id),
--     FOREIGN KEY (anime_id) REFERENCES anime_table(anime_id),
--     FOREIGN KEY (list_id) REFERENCES list_table(list_id)
-- );

DROP TABLE IF EXISTS list_anime;

CREATE TABLE IF NOT EXISTS list_anime(
    list_id INTEGER NOT NULL,
    anime_id INTEGER NOT NULL,
    PRIMARY KEY(list_id,anime_id),
    FOREIGN KEY (list_id) REFERENCES list_table(list_id),
    FOREIGN KEY (anime_id) REFERENCES anime_table(anime_id)
);

INSERT INTO user_table (userName,email,password,role,createdAt) VALUES ('Fido', 'Fido.corgi@gmail.com', '321', 'ROLE_USER', '2021-12-01');

