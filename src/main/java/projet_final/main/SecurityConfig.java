package projet_final.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import projet_final.main.Repository.UserRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    private UserRepository repo;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
        //Ici, rajouter des mvcMatcher("/route-a-proteger").authenticated() pour protéger des pages
        .mvcMatchers("/Account/**").authenticated()
        .mvcMatchers("/admin/**").hasRole("ADMIN")
        .anyRequest().permitAll()
        .and().formLogin();
    }

}
