package projet_final.main.entity;

import java.sql.Timestamp;

public class NewList {
    
    private Integer list_id;
    private String listName;
    private User user;
    private Timestamp createdAt;
    
    public NewList(String listName, Timestamp createdAt) {
        this.listName = listName;
        this.createdAt = createdAt;
    }

    public NewList(Integer list_id, String listName, Timestamp createdAt) {
        this.list_id = list_id;
        this.listName = listName;
        this.createdAt = createdAt;
    }

    public NewList() {
    }

    public Integer getList_id() {
        return list_id;
    }

    public void setList_id(Integer list_id) {
        this.list_id = list_id;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "NewList [createdAt=" + createdAt + ", listName=" + listName + ", list_id=" + list_id + "]";
    }

}
