package projet_final.main.entity;

import java.sql.Timestamp;

public class Comment {
    
    private Integer  comment_id;
    private String  comment_text;
    private User user;
    private Anime anime;
    private Timestamp createdAt;
    
    public Comment(Integer comment_id, String comment_text, Timestamp createdAt) {
        this.comment_id = comment_id;
        this.comment_text = comment_text;
        this.createdAt = createdAt;
    }

    public Comment(String comment_text, Timestamp createdAt) {
        this.comment_text = comment_text;
        this.createdAt = createdAt;
    }

    public Comment() {
    }

    public Integer getComment_id() {
        return comment_id;
    }

    public void setComment_id(Integer comment_id) {
        this.comment_id = comment_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Anime getAnime() {
        return anime;
    }

    public void setAnime(Anime anime) {
        this.anime = anime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Comment [comment_id=" + comment_id + ", comment_text=" + comment_text + ", createdAt=" + createdAt
                + "]";
    }


}
