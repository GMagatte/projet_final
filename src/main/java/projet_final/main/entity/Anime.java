package projet_final.main.entity;

import java.sql.Timestamp;

public class Anime {
    private Integer anime_id;
    private String animeName;
    private Integer episodeNumber;
    private Integer seasonNumber;
    private String description;
    private Timestamp createdAt;
    
    public Anime(Integer anime_id, String animeName, Integer episodeNumber, Integer seasonNumber, String description,
            Timestamp createdAt) {
        this.anime_id = anime_id;
        this.animeName = animeName;
        this.episodeNumber = episodeNumber;
        this.seasonNumber = seasonNumber;
        this.description = description;
        this.createdAt = createdAt;
    }

    public Anime(String animeName, Integer episodeNumber, Integer seasonNumber, String description,
            Timestamp createdAt) {
        this.animeName = animeName;
        this.episodeNumber = episodeNumber;
        this.seasonNumber = seasonNumber;
        this.description = description;
        this.createdAt = createdAt;
    }

    public Anime() {
    }

    public Integer getAnime_id() {
        return anime_id;
    }

    public void setAnime_id(Integer anime_id) {
        this.anime_id = anime_id;
    }

    public String getAnimeName() {
        return animeName;
    }

    public void setAnimeName(String animeName) {
        this.animeName = animeName;
    }

    public Integer getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(Integer episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public Integer getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(Integer seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Anime [animeName=" + animeName + ", anime_id=" + anime_id + ", createdAt=" + createdAt
                + ", description=" + description + ", episodeNumber=" + episodeNumber + ", seasonNumber=" + seasonNumber
                + "]";
    }

}
