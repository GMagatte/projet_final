package projet_final.main.Interfaces;

import java.util.List;

import projet_final.main.entity.User;

public interface I_UserRepository {

    List<User> findAll();
    User findById(Integer id);
    User findByEmail(String email);
    boolean save(User user);
    boolean update(User user);
    boolean deleteById(Integer id);
}
