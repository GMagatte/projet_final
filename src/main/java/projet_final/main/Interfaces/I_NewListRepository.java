package projet_final.main.Interfaces;

import java.util.List;

import projet_final.main.entity.NewList;

public interface I_NewListRepository {
    List<NewList> findAll();
    NewList findById(Integer id);
    boolean save(NewList list);
    boolean update(NewList list);
    boolean deleteById(Integer id);
    NewList findListByUserId(Integer id);

}
