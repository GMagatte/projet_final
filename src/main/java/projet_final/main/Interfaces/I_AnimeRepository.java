package projet_final.main.Interfaces;

import java.util.List;

import projet_final.main.entity.Anime;

public interface I_AnimeRepository {
    
    List<Anime> findAll();
    Anime findById(Integer id);
    Anime findByName(String name);
    boolean save(Anime anime);
    boolean update(Anime anime);
    boolean deleteById(Integer id);

}
 