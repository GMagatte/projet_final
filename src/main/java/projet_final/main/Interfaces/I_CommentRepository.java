package projet_final.main.Interfaces;

import java.util.List;

import projet_final.main.entity.Comment;

public interface I_CommentRepository {

    List<Comment> findAll();
    Comment findById(Integer id);
    boolean save(Comment com);
    boolean update(Comment com);
    boolean deleteById(Integer id);
    Comment findCommentByUserId(Integer id);
    Comment findCommentByAnimeId(Integer id);
}
