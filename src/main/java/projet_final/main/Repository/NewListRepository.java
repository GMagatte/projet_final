package projet_final.main.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import projet_final.main.Interfaces.I_NewListRepository;
import projet_final.main.entity.NewList;

@Repository
public class NewListRepository implements I_NewListRepository {
    @Autowired
    private DataSource dataSource;
    private Connection cnx;

    @Override
    public List<NewList> findAll() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM list_table");
            ResultSet result = stmt.executeQuery();
            List<NewList> list = new ArrayList<>();
            while (result.next()) {
                NewList newlist = new NewList(
                    result.getInt("list_id"),
                    result.getString("listName"),
                    result.getTimestamp("createdAt")
                );
                list.add(newlist);
            } return list;

        } catch (SQLException e) {
         e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public NewList findById(Integer id) {
         try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM list_table WHERE list_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                NewList list = new NewList(
                    result.getInt("list_id"),
                    result.getString("listName"),
                    result.getTimestamp("createdAt")
                );
                return list;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public boolean save(NewList list) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("INSERT INTO list_table (listName) VALUES (?)");
            stmt.setString(1, list.getListName());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean update(NewList list) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("UPDATE list_table SET listName=? WHERE list_id = ?");
            stmt.setString(1, list.getListName());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("DELETE FROM list_table WHERE list_id =?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() ==  1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public NewList findListByUserId(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM list_table");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                NewList list = new NewList(
                result.getInt("list_id"),
                result.getString("listName"),
                result.getTimestamp("createdAt")
                );
                return list;
            } 
       } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }
    
}
