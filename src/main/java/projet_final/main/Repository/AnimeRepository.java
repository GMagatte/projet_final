package projet_final.main.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import projet_final.main.Interfaces.I_AnimeRepository;
import projet_final.main.entity.Anime;

@Repository
public class AnimeRepository implements I_AnimeRepository {
    @Autowired
    private DataSource dataSource;
    private Connection cnx;

    @Override
    public List<Anime> findAll() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM anime_table");
            ResultSet result = stmt.executeQuery();
            List<Anime> animeList = new ArrayList<>();
            while (result.next()) {
                Anime anime = new Anime(
                    result.getInt("anime_id"),
                    result.getString("animeName"),
                    result.getInt("episodeNumber"),
                    result.getInt("seasonNumber"),
                    result.getString("description"),
                    result.getTimestamp("createdAt")
                );
                animeList.add(anime);
            } return animeList;
        } catch (SQLException e) {
         e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public Anime findById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM anime_table WHERE anime_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Anime anime = new Anime(
                    result.getInt("anime_id"),
                    result.getString("animeName"),
                    result.getInt("episodeNumber"),
                    result.getInt("seasonNumber"),
                    result.getString("description"),
                    result.getTimestamp("createdAt")
                );
                return anime;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public Anime findByName(String name) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM anime_table WHERE animeName=?");
            stmt.setString(1, name);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Anime anime = new Anime(
                    result.getInt("anime_id"),
                    result.getString("animeName"),
                    result.getInt("episodeNumber"),
                    result.getInt("seasonNumber"),
                    result.getString("description"),
                    result.getTimestamp("createdAt")
                );
                return anime;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public boolean save(Anime anime) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("INSERT INTO anime_table (animeName, episodeNumber, seasonNumber, description) VALUES (? , ? , ?, ?)");
            stmt.setString(1, anime.getAnimeName());
            stmt.setInt(2, anime.getEpisodeNumber());
            stmt.setInt(3, anime.getSeasonNumber());
            stmt.setString(4, anime.getDescription());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean update(Anime anime) {
      try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("UPDATE anime_table SET animeName=?, episodeNumber=?, seasonNumber=?, description=? WHERE anime_id = ?");
            stmt.setString(1, anime.getAnimeName());
            stmt.setInt(2, anime.getEpisodeNumber());
            stmt.setInt(3, anime.getSeasonNumber());
            stmt.setString(4, anime.getDescription());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("DELETE FROM anime_table WHERE anime_id =?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() ==  1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }
    
}
