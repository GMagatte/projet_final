package projet_final.main.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import projet_final.main.Interfaces.I_UserRepository;
import projet_final.main.entity.User;

@Repository
public class UserRepository implements I_UserRepository, UserDetailsService{

    @Autowired
    private DataSource dataSource;
    private Connection cnx;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    @Override
    public List<User> findAll() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM user_table");
            ResultSet result = stmt.executeQuery();
            List<User> userList = new ArrayList<>();
            while (result.next()) {
                User user = new User(
                    result.getInt("user_id"),
                    result.getString("userName"),
                    result.getString("email"),
                    result.getString("password"),
                    result.getString("role"),
                    result.getDate("createdAt").toLocalDate()
                    );
                userList.add(user);
            } return userList;
            
        } catch (SQLException e) {
         e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public User findById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM user_table WHERE user_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(
                    result.getInt("user_id"), 
                    result.getString("userName"),
                    result.getString("email"),
                    result.getString("password"),
                    result.getString("role"),
                    result.getDate("createdAt").toLocalDate()
                );
                return user;
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public User findByEmail(String email) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM user_table WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(
                    result.getInt("user_id"), 
                    result.getString("userName"),
                    result.getString("email"),
                    result.getString("password"),
                    result.getString("role"),
                    result.getDate("createdAt").toLocalDate()
                );
                return user;
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public boolean save(User user) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("INSERT INTO user_table (userName, email, password) VALUES (? , ? , ?)");
            stmt.setString(1, user.getUserName());
            stmt.setString(2, user.getEmail());
            stmt.setString(3, user.getPassword());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean update(User user) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("UPDATE user_table SET userName=?, email=?, password=? WHERE user_id = ?");
            stmt.setString(1, user.getUserName());
            stmt.setString(2, user.getEmail());
            stmt.setString(3, user.getPassword());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("DELETE FROM user_table WHERE user_id =?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() ==  1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
    
}