package projet_final.main.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import projet_final.main.Interfaces.I_CommentRepository;
import projet_final.main.entity.Comment;

@Repository
public class CommentRepository implements I_CommentRepository {
    @Autowired
    private DataSource dataSource;
    private Connection cnx;
         
    @Override
    public List<Comment> findAll() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM comment_table");
            ResultSet result = stmt.executeQuery();
            List<Comment> commentList = new ArrayList<>();
            while (result.next()) {
                Comment com = new Comment(
                    result.getInt("comment_id"),
                    result.getString("comment_text"),
                    result.getTimestamp("createdAt")
                );
                commentList.add(com);
            } return commentList;
            
        } catch (SQLException e) {
         e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public Comment findById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM comment_table WHERE comment_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Comment com = new Comment(
                    result.getInt("comment_id"),
                    result.getString("comment_text"),
                    result.getTimestamp("createdAt")
                );
                return com;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public boolean save(Comment com) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("INSERT INTO comment_table (comment_text) VALUES (?)");
            stmt.setString(1, com.getComment_text());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean update(Comment com) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("UPDATE comment_table SET comment_text=? WHERE comment_id = ?");
            stmt.setString(1, com.getComment_text());
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("DELETE FROM comment_table WHERE comment_id =?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() ==  1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return false;
    }

    @Override
    public Comment findCommentByUserId(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM comment_table WHERE user_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Comment com = new Comment(
                    result.getInt("comment_id"),
                    result.getString("comment_text"),
                    result.getTimestamp("createdAt")
                );
                return com;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }

    @Override
    public Comment findCommentByAnimeId(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
            .prepareStatement("SELECT * FROM comment_table WHERE anime_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Comment com = new Comment(
                    result.getInt("comment_id"),
                    result.getString("comment_text"),
                    result.getTimestamp("createdAt")
                );
                return com;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        DataSourceUtils.releaseConnection(cnx, dataSource);}
        return null;
    }
    
}
