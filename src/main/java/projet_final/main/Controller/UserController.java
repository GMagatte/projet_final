package projet_final.main.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/Account")
public class UserController {
    
    @GetMapping()
    public String showUserAccount() {
        return "Account";
    }
    
}
