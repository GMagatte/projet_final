package projet_final.main.Repository;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.security.core.userdetails.User;

public class UserRepositoryTest {

    
    UserRepository userRepository;
    private projet_final.main.entity.User user;

    @BeforeEach
    void init() {
        userRepository = new UserRepository();
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("database.sql")
                .build();
        userRepository.setDataSource(dataSource);
    }

    @Test
    void testDeleteById() {
        assertTrue(userRepository.deleteById(1));
    }

    @Test
    void testFindAll() {
        assertNotEquals(0, userRepository.findAll().size());
    }

    @Test
    void testFindByEmail() {
        assertNotNull(userRepository.findByEmail("Fido.corgi@gmail.com"));
    }

    @Test
    void testFindById() {
        assertNotNull(userRepository.findById(1));
    }

    @Test
    void testSave() {
        // User user = new projet_final.main.entity.User()
        // assertTrue(userRepository.save(user));
        // assertNotNull(user.getId());
    }

    @Test
    void testUpdate() {
    //     User track = new User('username', 'password', 'authorities')
    //     track.setId(1);
    //     assertTrue(userRepository.update(user));
    }
}
